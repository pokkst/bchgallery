package xyz.pokkst.gallerybch.util;

public class Constants {
    public static final String WALLET_NAME = "users_wallet";
    public static boolean IS_PRODUCTION = true;
    public static int CASH_ACCOUNT_GENESIS_MODIFIED = 563620;
}
