package xyz.pokkst.gallerybch.uri;

import org.bitcoinj.utils.MonetaryFormat;

import java.net.URL;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import xyz.pokkst.gallerybch.MainActivity;

public class URIHelper {

    public void handleURI(String address) {
        MainActivity.INSTANCE.uiHelper.sendType.setSelection(0);
        MainActivity.INSTANCE.walletHelper.sendType = MonetaryFormat.CODE_BTC;
        MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply();
        System.out.println("This is a URI. Getting variables.");
        Map<String, List<String>> mappedVariables = getQueryParams(address);
        System.out.println("All variables $mappedVariables");
        MainActivity.INSTANCE.uiHelper.displayRecipientAddress(getQueryBaseAddress(address));
        String amount = mappedVariables.get("amount").get(0);
        System.out.println(mappedVariables);
        DecimalFormat df = new DecimalFormat("#.########");

        MainActivity.INSTANCE.uiHelper.etAmount_AM.setText(df.format(java.lang.Double.parseDouble(amount)).replace(",", ""));
        System.out.println("Amount to pay: " + df.format(java.lang.Double.parseDouble(amount)));
    }

    public Map<String, List<String>> getQueryParams(String url) {
        try {
            final Map<String, List<String>> query_pairs = new LinkedHashMap<String, List<String>>();
            final String[] urlParts = url.split("\\?");
            final String[] pairs = urlParts[1].split("&");
            for (String pair : pairs) {
                final int idx = pair.indexOf("=");
                final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), "UTF-8") : pair;
                if (!query_pairs.containsKey(key)) {
                    query_pairs.put(key, new LinkedList<String>());
                }
                final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), "UTF-8") : null;
                query_pairs.get(key).add(value);
            }

            return query_pairs;
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getQueryBaseAddress(String url) {
        String[] urlParts = url.split("\\?");
        return urlParts[0];
    }

}