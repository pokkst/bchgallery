package xyz.pokkst.gallerybch.net;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.Scanner;

public class NetHelper {

    public double getBchPriceUsd()
    {
        try {
            String out = new Scanner(new URL("https://api.coinmarketcap.com/v1/ticker/bitcoin-cash/").openStream(), "UTF-8").useDelimiter("\\A").next();
            JSONArray json = new JSONArray(out);
            return Double.parseDouble(json.getJSONObject(0).getString("price_usd"));
        } catch (Exception e) {
            e.printStackTrace();

            return 0;
        }
    }

    public double getBchPriceEur()
    {
        try {
            String out = new Scanner(new URL("https://bitpay.com/rates/BCH/EUR").openStream(), "UTF-8").useDelimiter("\\A").next();
            JSONObject json = new JSONObject(out);
            return json.getJSONObject("data").getDouble("rate");
        } catch (Exception e) {
            e.printStackTrace();

            return 0;
        }
    }
}
