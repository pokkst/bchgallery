package xyz.pokkst.gallerybch.ui;

import android.content.Context;
import android.util.AttributeSet;

/**
 * Original:
 * An EditText, which notifies when something was cut/copied/pasted inside it.
 * @author Lukas Knuth
 * @version 1.0
 *
 * Update:
 * Added a OnCutCopyPasteListener so this class can be used as a plug&play component
 * @author Guillermo Muntaner on 14/01/16.
 *
 * Source & discussion:
 * https://stackoverflow.com/questions/14980227/android-intercept-paste-copy-cut-on-edittext
 */
public class RecipientEditText extends android.support.v7.widget.AppCompatEditText {

    public interface RecipientTextListener {
        void onUpdate();
    }

    private RecipientTextListener listener;

    /**
     * Set a OnCutCopyPasteListener.
     * @param listener
     */
    public void setOnCutCopyPasteListener(RecipientTextListener listener) {
        this.listener = listener;
    }

    /*
        Just the constructors to create a new EditText...
     */
    public RecipientEditText(Context context) {
        super(context);
    }

    public RecipientEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RecipientEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * <p>This is where the "magic" happens.</p>
     * <p>The menu used to cut/copy/paste is a normal ContextMenu, which allows us to
     *  overwrite the consuming method and react on the different events.</p>
     * @see <a href="http://grepcode.com/file/repository.grepcode.com/java/ext/com.google.android/android/2.3_r1/android/widget/TextView.java#TextView.onTextContextMenuItem%28int%29">Original Implementation</a>
     */
    @Override
    public boolean onTextContextMenuItem(int id) {
        // Do your thing:
        boolean consumed = super.onTextContextMenuItem(id);
        // React:
        switch (id){
            case android.R.id.cut:
                onCut();
                break;
            case android.R.id.copy:
                onCopy();
                break;
            case android.R.id.paste:
                onPaste();
        }
        return consumed;
    }

    /**
     * Text was cut from this EditText.
     */
    public void onCut(){

    }

    /**
     * Text was copied from this EditText.
     */
    public void onCopy(){

    }

    /**
     * Text was pasted into the EditText.
     */
    public void onPaste(){
        if(listener !=null)
            listener.onUpdate();
    }
}