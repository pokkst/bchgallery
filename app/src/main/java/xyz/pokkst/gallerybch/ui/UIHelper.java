package xyz.pokkst.gallerybch.ui;

import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.nfc.FormatException;
import android.os.CountDownTimer;
import android.os.Looper;
import android.provider.ContactsContract;
import android.support.annotation.ColorRes;
import android.support.annotation.StringRes;
import android.support.annotation.UiThread;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.AddressFactory;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.TransactionConfidence;
import org.bitcoinj.net.NetHelper;
import org.bitcoinj.protocols.payments.PaymentProtocolException;
import org.bitcoinj.protocols.payments.PaymentSession;
import org.bitcoinj.utils.MonetaryFormat;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.Wallet;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import xyz.pokkst.gallerybch.MainActivity;
import xyz.pokkst.gallerybch.R;
import xyz.pokkst.gallerybch.crypto.HashHelper;
import xyz.pokkst.gallerybch.json.JSONHelper;
import xyz.pokkst.gallerybch.qr.QRHelper;
import xyz.pokkst.gallerybch.uri.URIHelper;
import xyz.pokkst.gallerybch.util.Constants;
import xyz.pokkst.gallerybch.wallet.WalletHelper;

public class UIHelper {

    public FrameLayout photosBox;
    public FrameLayout infoPage;
    public TextView infoText;
    public Button btnCloseAbout;
    public Switch offlineSwitch;
    public Switch torSwitch;
    public Switch nodeSwitch;
    public EditText nodeET;
    public Button btnWriteNFC;
    public EditText txtHex;
    public FrameLayout txInfo;
    public TextView txInfoTV;
    public Button btnCloseTx;
    public Button btnViewTx;
    public FrameLayout walletSettings;
    public TextView xpub;
    public TextView seed;
    public EditText newSeed;
    public Button btnRecover;
    public Button btnClose;
    public FrameLayout passcode;
    public TextView titleLabel;
    public TextView pinDisplay;
    public Button button1;
    public Button button2;
    public Button button3;
    public Button button4;
    public Button button5;
    public Button button6;
    public Button button7;
    public Button button8;
    public Button button9;
    public Button button0;
    public Button btnCancel;
    public Button btnEnter;
    public FrameLayout flDownloadContent_LDP;
    public ProgressBar pbProgress_LDP;
    public TextView tvPercentage_LDP;
    public Toolbar toolbar_AT;
    public SwipeRefreshLayout srlContent_AM;
    public TextView tvMyBalance_AM;
    public TextView tvMyAddress_AM;
    public RecipientEditText tvRecipientAddress_AM;
    public TextView etAmount_AM;
    public TextView etFee_AM;
    public Button btnSend_AM;
    public TextView sendTitle;
    public ImageView ivCopy_AM;
    public ImageView qrCode;
    public ImageView btcLogo;
    public NonScrollListView lv_txList;
    public Button sendTab;
    public Button receiveTab;
    public Button txTab;
    public Button nfcTab;
    public LinearLayout sendWindow;
    public LinearLayout receiveWindow;
    public LinearLayout txWindow;
    public LinearLayout nfcWindow;
    public String strScanRecipientQRCode;
    public Button setMaxCoins;
    public Button btnCashAcctSetup;
    public Button btnNewAcct;
    public Button btnExistingAcct;
    public FrameLayout cashAcctSetup;
    public Button btnCloseCashAcct;
    public Button btnCloseNewAcct;
    public Button btnCloseExistingAcct;
    public FrameLayout newCashAcct;
    public FrameLayout existingCashAcct;
    public Button btnRegisterCashAcct;
    public Button btnSaveAcct;
    public TextView newCashAcctName;
    public TextView existingCashAcctName;
    public TextView tv_cashAcctAddress;
    public ImageView ivCopy_CashAcct;
    public ImageButton contactsBtn;
    public Spinner sendType;

    public UIHelper()
    {
        MainActivity mainActivity = MainActivity.INSTANCE;
        photosBox = mainActivity.findViewById(R.id.photosBox);
        infoPage = mainActivity.findViewById(R.id.infoPage);
        infoText = mainActivity.findViewById(R.id.infoText);
        btnCloseAbout = mainActivity.findViewById(R.id.btnCloseAbout);
        offlineSwitch = mainActivity.findViewById(R.id.offlineSwitch);
        torSwitch = mainActivity.findViewById(R.id.torSwitch);
        nodeSwitch = mainActivity.findViewById(R.id.nodeSwitch);
        nodeET = mainActivity.findViewById(R.id.nodeET);
        btnWriteNFC = mainActivity.findViewById(R.id.btnWriteNFC);
        txtHex = mainActivity.findViewById(R.id.txtHex);
        newSeed = mainActivity.findViewById(R.id.newSeed);
        btnRecover = mainActivity.findViewById(R.id.btnRecover);
        btnClose = mainActivity.findViewById(R.id.btnClose);
        btcLogo = mainActivity.findViewById(R.id.btcLogo);
        tvMyBalance_AM = mainActivity.findViewById(R.id.tvMyBalance_AM);
        passcode = mainActivity.findViewById(R.id.passcode);
        pinDisplay = mainActivity.findViewById(R.id.pinDisplay);
        button1 = mainActivity.findViewById(R.id.button1);
        button2 = mainActivity.findViewById(R.id.button2);
        button3 = mainActivity.findViewById(R.id.button3);
        button4 = mainActivity.findViewById(R.id.button4);
        button5 = mainActivity.findViewById(R.id.button5);
        button6 = mainActivity.findViewById(R.id.button6);
        button7 = mainActivity.findViewById(R.id.button7);
        button8 = mainActivity.findViewById(R.id.button8);
        button9 = mainActivity.findViewById(R.id.button9);
        button0 = mainActivity.findViewById(R.id.button0);
        btnCancel = mainActivity.findViewById(R.id.btnCancel);
        btnEnter = mainActivity.findViewById(R.id.btnEnter);
        tvMyAddress_AM = mainActivity.findViewById(R.id.tvMyAddress_AM);
        etFee_AM = mainActivity.findViewById(R.id.etFee_AM);
        sendTitle = mainActivity.findViewById(R.id.sendTitle);
        lv_txList = mainActivity.findViewById(R.id.lv_txList);
        txTab = mainActivity.findViewById(R.id.txTab);
        nfcTab = mainActivity.findViewById(R.id.nfcTab);
        txWindow = mainActivity.findViewById(R.id.txWindow);
        nfcWindow = mainActivity.findViewById(R.id.nfcWindow);
        walletSettings = mainActivity.findViewById(R.id.walletSettings);
        seed = mainActivity.findViewById(R.id.seed);
        xpub = mainActivity.findViewById(R.id.xpub);
        titleLabel = mainActivity.findViewById(R.id.titleLabel);
        flDownloadContent_LDP = mainActivity.findViewById(R.id.flDownloadContent_LDP);
        pbProgress_LDP = mainActivity.findViewById(R.id.pbProgress_LDP);
        tvPercentage_LDP = mainActivity.findViewById(R.id.tvPercentage_LDP);
        srlContent_AM = mainActivity.findViewById(R.id.srlContent_AM);
        tvRecipientAddress_AM = mainActivity.findViewById(R.id.tvRecipientAddress_AM);
        etAmount_AM = mainActivity.findViewById(R.id.etAmount_AM);
        btnSend_AM = mainActivity.findViewById(R.id.btnSend_AM);
        ivCopy_AM = mainActivity.findViewById(R.id.ivCopy_AM);
        qrCode = mainActivity.findViewById(R.id.qrCode);
        sendTab = mainActivity.findViewById(R.id.sendTab);
        receiveTab = mainActivity.findViewById(R.id.receiveTab);
        sendWindow = mainActivity.findViewById(R.id.sendWindow);
        receiveWindow = mainActivity.findViewById(R.id.receiveWindow);
        txInfo = mainActivity.findViewById(R.id.txInfo);
        txInfoTV = mainActivity.findViewById(R.id.txInfoTV);
        btnCloseTx = mainActivity.findViewById(R.id.btnCloseTx);
        btnViewTx = mainActivity.findViewById(R.id.btnViewTx);
        strScanRecipientQRCode = mainActivity.getResources().getString(R.string.scan_recipient_qr);
        toolbar_AT = mainActivity.findViewById(R.id.toolbar_AT);
        setMaxCoins = mainActivity.findViewById(R.id.setMaxCoins);
        btnCashAcctSetup = mainActivity.findViewById(R.id.btnCashAcctSetup);
        btnNewAcct = mainActivity.findViewById(R.id.btnNewAcct);
        btnExistingAcct = mainActivity.findViewById(R.id.btnExistingAcct);
        cashAcctSetup = mainActivity.findViewById(R.id.cashAcctSetup);
        btnCloseCashAcct = mainActivity.findViewById(R.id.btnCloseCashAcct);
        btnCloseNewAcct = mainActivity.findViewById(R.id.btnCloseNewAcct);
        btnCloseExistingAcct = mainActivity.findViewById(R.id.btnCloseExistingAcct);
        newCashAcct = mainActivity.findViewById(R.id.newCashAcct);
        existingCashAcct = mainActivity.findViewById(R.id.existingCashAcct);
        btnRegisterCashAcct = mainActivity.findViewById(R.id.btnRegisterCashAcct);
        btnSaveAcct = mainActivity.findViewById(R.id.btnSaveAcct);
        existingCashAcctName = mainActivity.findViewById(R.id.existingCashAcctName);
        tv_cashAcctAddress = mainActivity.findViewById(R.id.tv_cashAcctAddress);
        ivCopy_CashAcct = mainActivity.findViewById(R.id.ivCopy_CashAcct);
        newCashAcctName = mainActivity.findViewById(R.id.newCashAcctName);
        contactsBtn = mainActivity.findViewById(R.id.contactsBtn);
        sendType = mainActivity.findViewById(R.id.sendType);

        this.setListeners();

    }

    private void setListeners() {
        srlContent_AM.setOnRefreshListener(() -> refresh());
        btnSend_AM.setOnClickListener(v -> MainActivity.INSTANCE.walletHelper.send());
        toolbar_AT.setOnClickListener(v -> clickImage());
        ivCopy_AM.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager)MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("My wallet address", tvMyAddress_AM.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show();
        });

        ivCopy_CashAcct.setOnClickListener(v -> {
            ClipboardManager clipboard = (ClipboardManager)MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("My Cash Account", tv_cashAcctAddress.getText().toString());
            clipboard.setPrimaryClip(clip);
            Toast.makeText(MainActivity.INSTANCE, "Copied", Toast.LENGTH_SHORT).show();
        });

        lv_txList.setOnItemClickListener((parent, view, position, id) -> {
            Transaction tx = MainActivity.INSTANCE.txList.get(position);
            displayTxWindow(tx);
        });

        sendTab.setOnClickListener(v -> displaySend());
        receiveTab.setOnClickListener(v -> displayReceive());
        txTab.setOnClickListener(v -> displayTx());
        nfcTab.setOnClickListener(v -> displayNFC());

        button1.setOnClickListener(v -> addToPIN(1));
        button2.setOnClickListener(v -> addToPIN(2));
        button3.setOnClickListener(v -> addToPIN(3));
        button4.setOnClickListener(v -> addToPIN(4));
        button5.setOnClickListener(v -> addToPIN(5));
        button6.setOnClickListener(v -> addToPIN(6));
        button7.setOnClickListener(v -> addToPIN(7));
        button8.setOnClickListener(v -> addToPIN(8));
        button9.setOnClickListener(v -> addToPIN(9));
        button0.setOnClickListener(v -> addToPIN(0));
        btnCancel.setOnClickListener(v -> cancelPIN());
        contactsBtn.setOnClickListener(v-> showContactSelectionScreen());

        btnWriteNFC.setOnClickListener(v -> {
            if(MainActivity.INSTANCE.nfcHelper.getNfcAdapter() != null) {
                try {
                    if (MainActivity.INSTANCE.nfcHelper.getNfcTag() == null) {
                        Toast.makeText(MainActivity.INSTANCE, "No NFC tag was found!", Toast.LENGTH_LONG).show();
                    } else {
                        MainActivity.INSTANCE.nfcHelper.write(txtHex.getText().toString(), MainActivity.INSTANCE.nfcHelper.getNfcTag());
                        Toast.makeText(MainActivity.INSTANCE, "Successfully wrote hex to NFC!", Toast.LENGTH_LONG).show();
                    }
                } catch (IOException e) {
                    Toast.makeText(MainActivity.INSTANCE, "An error occurred.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                } catch (FormatException e) {
                    Toast.makeText(MainActivity.INSTANCE, "Error formatting.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });

        btnCashAcctSetup.setOnClickListener(v -> cashAcctSetup.setVisibility(View.VISIBLE));
        btnCloseCashAcct.setOnClickListener(v -> cashAcctSetup.setVisibility(View.GONE));
        btnNewAcct.setOnClickListener(v -> newCashAcct.setVisibility(View.VISIBLE));
        btnCloseNewAcct.setOnClickListener(v -> newCashAcct.setVisibility(View.GONE));
        btnExistingAcct.setOnClickListener(v -> existingCashAcct.setVisibility(View.VISIBLE));
        btnCloseExistingAcct.setOnClickListener(v -> existingCashAcct.setVisibility(View.GONE));
        btnRegisterCashAcct.setOnClickListener(v -> this.registerCashAccount());
        btnSaveAcct.setOnClickListener(v -> this.saveCashAccount());
        setMaxCoins.setOnClickListener(v -> this.setMaxCoins());


        tvRecipientAddress_AM.setOnCutCopyPasteListener(() -> {
            ClipboardManager clipboard = (ClipboardManager) MainActivity.INSTANCE.getSystemService(Context.CLIPBOARD_SERVICE);
            String clipboardText = clipboard.getText().toString();

            if(!TextUtils.isEmpty(clipboardText))
                MainActivity.INSTANCE.walletHelper.processScanOrPaste(clipboardText);
        });
    }

    public void initSendType()
    {
        List<String> items = new ArrayList<String>();
        items.add("BCH");
        items.add("USD");
        items.add("EUR");
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.INSTANCE, R.layout.spinner_item, items);
        sendType.setAdapter(adapter);

        switch(MainActivity.INSTANCE.walletHelper.sendType){
            case "BCH": sendType.setSelection(0); break;
            case "USD": sendType.setSelection(1); break;
            case "EUR": sendType.setSelection(2); break;
        }

        sendType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: MainActivity.INSTANCE.walletHelper.sendType = "BCH"; break;
                    case 1: MainActivity.INSTANCE.walletHelper.sendType = "USD"; break;
                    case 2: MainActivity.INSTANCE.walletHelper.sendType = "EUR"; break;
                }

                MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply();
                System.out.println(MainActivity.INSTANCE.walletHelper.sendType);

                if(MainActivity.INSTANCE.walletHelper.getWalletKit() != null)
                    refresh();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public String registeredTxHash = "null";
    public String registeredBlock = "null";
    private String registeredBlockHash = "null";
    public CountDownTimer timer;

    private void showContactSelectionScreen()
    {
        Intent pickContact = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        pickContact.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        MainActivity.INSTANCE.startActivityForResult(pickContact, 14);
    }

    private void registerCashAccount()
    {
        new Thread()
        {
            @Override
            public void run()
            {
                String name = newCashAcctName.getText().toString();

                if(!name.contains("#")) {
                    JSONObject json = new JSONObject();

                    WalletHelper walletHelper = MainActivity.INSTANCE.walletHelper;
                    String cashAddrStr = walletHelper.getWallet().currentReceiveAddress().toString();

                    try {
                        json.put("name", name);

                        JSONArray paymentsArray = new JSONArray();
                        paymentsArray.put(cashAddrStr);

                        json.put("payments", paymentsArray);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String requestUrl = "https://cashacct.imaginary.cash/register/";
                    URL url = null;
                    try {
                        url = new URL(requestUrl);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    HttpURLConnection connection = null;

                    try {
                        connection = (HttpURLConnection) url.openConnection();
                        connection.setDoOutput(true);
                        connection.setDoInput(true);
                        connection.setInstanceFollowRedirects(false);
                        connection.setRequestMethod("POST");
                        connection.setRequestProperty("Content-Type", "application/json");
                        connection.setRequestProperty("charset", "utf-8");
                        connection.setRequestProperty("Accept", "application/json");
                        connection.setRequestProperty("Content-Length", Integer.toString(json.toString().getBytes().length));
                        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0");

                        connection.setUseCaches(false);

                        connection.setConnectTimeout(60000);
                        connection.setReadTimeout(60000);

                        connection.connect();

                        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                        wr.write(json.toString().getBytes());
                        wr.flush();
                        wr.close();

                        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                        String line = "";
                        StringBuilder res = new StringBuilder();
                        while ((line = rd.readLine()) != null) {
                            res.append(line);
                        }

                        wr.flush();
                        wr.close();


                        String responseJson = res.toString();
                        System.out.println(responseJson);

                        JSONHelper jsonHelper = new JSONHelper();

                        registeredTxHash = jsonHelper.getRegisterTxHash(responseJson);
                        System.out.println(registeredTxHash);
                        MainActivity.INSTANCE.prefs.edit().putString("cashAccount", newCashAcctName.getText().toString() + "#???").apply();
                        MainActivity.INSTANCE.prefs.edit().putString("cashAcctTx", registeredTxHash).apply();
                        MainActivity.INSTANCE.runOnUiThread(() -> {
                            newCashAcct.setVisibility(View.GONE);
                            cashAcctSetup.setVisibility(View.GONE);
                            displayCashAccount(newCashAcctName.getText().toString() + "#???");
                        });

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else
                {
                    MainActivity.INSTANCE.runOnUiThread(() -> showToastMessage("Do not set an account number!"));
                }
            }

        }.start();

        //Every 5 minutes, we check if a block has been found
        timer = new CountDownTimer(150000, 20) {

            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                try{
                    checkForAccountIdentity();
                }catch(Exception e){
                    Log.e("Error", "Error: " + e.toString());
                }
            }
        }.start();
    }

    public void initialAccountIdentityCheck(String name)
    {
        new Thread(){
            @Override
            public void run()
            {
                if(registeredBlock.equals("null") || registeredBlock.equals("???")) {
                    if (registeredTxHash.equals("null")) {
                        System.out.println("No response from registration server...");
                    } else {
                        JSONHelper jsonHelper = new JSONHelper();
                        registeredBlock = jsonHelper.getTransactionsBlock(registeredTxHash);

                        if(registeredBlock.equals("???")) {
                            System.out.println("Block not found... checking in 5 minutes.");
                        }
                        else {
                            registeredBlockHash = jsonHelper.getTransactionsBlockHash(registeredTxHash);
                            int accountIdentity = Integer.parseInt(registeredBlock) - Constants.CASH_ACCOUNT_GENESIS_MODIFIED;

                            HashHelper hashHelper = new HashHelper();
                            System.out.println("Block hash: " + registeredBlockHash);
                            System.out.println("Block tx hash: " + registeredTxHash);
                            String collisionIdentifier = hashHelper.getCashAccountCollision(registeredBlockHash, registeredTxHash);
                            System.out.println(name + "#" + accountIdentity + "." + collisionIdentifier);
                            String identifier = jsonHelper.getCashAccountIdentifier(name + "#" + accountIdentity + "." + collisionIdentifier);
                            MainActivity.INSTANCE.prefs.edit().putString("cashAccount", identifier).apply();
                            System.out.println(identifier);

                            if(!isDisplayingDownload() && MainActivity.INSTANCE.walletHelper.getWalletKit() != null)
                                MainActivity.INSTANCE.runOnUiThread(UIHelper.this::refresh);
                        }
                    }
                }
            }
        }.start();
    }

    public void checkForAccountIdentity()
    {
        new Thread(){
            @Override
            public void run()
            {
                if(registeredBlock.equals("null") || registeredBlock.equals("???")) {
                    if (registeredTxHash.equals("null")) {
                        System.out.println("No response from registration server...");
                        timer.start();
                    } else {
                        JSONHelper jsonHelper = new JSONHelper();
                        registeredBlock = jsonHelper.getTransactionsBlock(registeredTxHash);

                        if(registeredBlock.equals("???")) {
                            System.out.println("Block not found... checking in 5 minutes.");
                            timer.start();
                        }
                        else {
                            registeredBlockHash = jsonHelper.getTransactionsBlockHash(registeredTxHash);
                            int accountIdentity = Integer.parseInt(registeredBlock) - Constants.CASH_ACCOUNT_GENESIS_MODIFIED;
                            String collisionIdentifier = new HashHelper().getCashAccountCollision(registeredBlockHash, registeredTxHash);
                            String identifier = jsonHelper.getCashAccountIdentifier(newCashAcctName.getText().toString() + "#" + accountIdentity + "." + collisionIdentifier);
                            MainActivity.INSTANCE.prefs.edit().putString("cashAccount", identifier).apply();
                            System.out.println(identifier);

                            if(!isDisplayingDownload() && MainActivity.INSTANCE.walletHelper.getWalletKit() != null)
                                MainActivity.INSTANCE.runOnUiThread(UIHelper.this::refresh);
                        }
                    }
                }
            }
        }.start();
    }

    private void saveCashAccount() {
        String accountName = existingCashAcctName.getText().toString();

        if(accountName.contains("#"))
        {
            new Thread() {
                @Override
                public void run() {
                    String cashAcctAddr;
                    try {
                        cashAcctAddr = new NetHelper().getCashAccountAddress(MainActivity.INSTANCE.walletHelper.parameters, accountName);

                        cashAcctAddr = cashAcctAddr.replace(Constants.IS_PRODUCTION ? "bitcoincash:" : "bchtest:", "");

                        System.out.println(cashAcctAddr);
                        Address accountAddress;

                        if(cashAcctAddr.startsWith("q")) {
                            accountAddress = AddressFactory.create().getAddress(MainActivity.INSTANCE.walletHelper.parameters, cashAcctAddr);
                        }
                        else {
                            accountAddress = Address.fromBase58(MainActivity.INSTANCE.walletHelper.parameters, cashAcctAddr);
                        }

                        boolean isAddressMine = MainActivity.INSTANCE.walletHelper.isAddressMine(accountAddress.toString());

                        if(isAddressMine)
                        {
                            MainActivity.INSTANCE.prefs.edit().putString("cashAccount", accountName).apply();

                            MainActivity.INSTANCE.runOnUiThread(() -> {
                                existingCashAcct.setVisibility(View.GONE);
                                cashAcctSetup.setVisibility(View.GONE);
                                refresh();
                            });
                        }
                        else
                        {
                            MainActivity.INSTANCE.runOnUiThread(() -> showToastMessage("This is not your Cash Account!"));
                        }
                    }
                    catch (NullPointerException e)
                    {
                        MainActivity.INSTANCE.runOnUiThread(() -> showToastMessage("Cash Account not found."));
                    }
                }
            }.start();
        }
        else
        {
            this.showToastMessage("Not a valid Cash Account.");
        }
    }

    public void getRecoveryWindow() {
        if(MainActivity.INSTANCE.walletHelper.getWalletKit() != null) {
            DeterministicSeed seed = MainActivity.INSTANCE.walletHelper.getWallet().getKeyChainSeed();
            final List<String> mnemonicCode = seed.getMnemonicCode();
            StringBuilder recoverySeed = new StringBuilder();

            assert mnemonicCode != null;
            for (int x = 0; x < mnemonicCode.size(); x++) {
                recoverySeed.append(mnemonicCode.get(x)).append(" ");
            }

            displayRecoveryWindow(recoverySeed.toString());
        }
        else
        {
            displayRecoveryWindow("");
        }
    }

    public void refresh() {
        Wallet wallet = MainActivity.INSTANCE.walletHelper.getWallet();
        MainActivity.INSTANCE.walletHelper.serializedXpub = wallet.getWatchingKey().serializePubB58(MainActivity.INSTANCE.walletHelper.parameters);

        this.displayMyBalance(MainActivity.INSTANCE.walletHelper.getBalance(wallet).toFriendlyString());

        if(MainActivity.INSTANCE.uiHelper.txWindow.getVisibility() == View.VISIBLE) {
            this.setArrayAdapter(wallet);
        }

        String myAddress = wallet.currentReceiveAddress().toString();
        this.displayMyAddress(myAddress, wallet);

        String cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "");

        if(cashAccount.equals(""))
        {
            btnCashAcctSetup.setVisibility(View.VISIBLE);
            tv_cashAcctAddress.setVisibility(View.GONE);
            ivCopy_CashAcct.setVisibility(View.GONE);
        }
        else
        {
            if(cashAccount.contains("#???")) {
                String cashAcctPlain = cashAccount.replace("#???", "");
                initialAccountIdentityCheck(cashAcctPlain);

            }

            btnCashAcctSetup.setVisibility(View.GONE);
            this.displayCashAccount(cashAccount);
        }
    }

    private void displayCashAccount(String cashAccount)
    {
        tv_cashAcctAddress.setVisibility(View.VISIBLE);
        ivCopy_CashAcct.setVisibility(View.VISIBLE);

        tv_cashAcctAddress.setText(cashAccount);
    }

    private void displayTxWindow(Transaction tx) {
        if(!isDisplayingDownload())
        {
            DecimalFormat decimalFormatter = new DecimalFormat("0.########");
            String receivedValueStr = MonetaryFormat.BTC.format(tx.getValue(MainActivity.INSTANCE.walletHelper.getWallet())).toString();
            receivedValueStr = receivedValueStr.replace("BCH ", "");
            float amtTransferred = Float.parseFloat(receivedValueStr);
            String amtTransferredStr = decimalFormatter.format(Math.abs(amtTransferred));
            String feeStr;

            if (tx.getFee() != null) {
                String feeValueStr = MonetaryFormat.BTC.format(tx.getFee()).toString();
                feeValueStr = feeValueStr.replace("BCH ", "");
                float fee = Float.parseFloat(feeValueStr);
                feeStr = decimalFormatter.format(fee);
            } else {
                feeStr = "n/a";
            }

            TransactionConfidence txConfirmations = tx.getConfidence();

            String txConfirms = "" + txConfirmations.getDepthInBlocks();
            String txDate = tx.getUpdateTime() + "";
            String txHash = tx.getHashAsString();
            txInfo.setVisibility(View.VISIBLE);
            txInfoTV.setText(Html.fromHtml("<b>BCH Transferred:</b> " + amtTransferredStr + "<br> <b>Fee:</b> " + feeStr + "<br> <b>Date:</b> " + txDate + "<br> <b>Confirmations:</b> " + txConfirms));
            btnCloseTx.setOnClickListener(v -> txInfo.setVisibility(View.GONE));
            btnViewTx.setOnClickListener(v -> {
                String url = Constants.IS_PRODUCTION ? "https://explorer.bitcoin.com/bch/tx/" : "https://explorer.bitcoin.com/tbch/tx/";
                Uri uri = Uri.parse(url + txHash); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                MainActivity.INSTANCE.startActivity(intent);
            });
        }
    }

    @UiThread
    public void displayDownloadContent(boolean isShown) {
        flDownloadContent_LDP.setVisibility(isShown ? View.VISIBLE : View.GONE);
    }

    public boolean isDisplayingDownload() {
        return flDownloadContent_LDP.getVisibility() == View.VISIBLE;
    }

    @UiThread
    public void displayProgress(int percent) {
        if(pbProgress_LDP.isIndeterminate()) pbProgress_LDP.setIndeterminate(false);
        pbProgress_LDP.setProgress(percent);
    }

    @UiThread
    public void displayPercentage(int percent) {
        tvPercentage_LDP.setText(String.valueOf(percent) + " %");

        /*to hopefully not crash, we check for the remainder when dividing by 4, and if it's 0, we update the tx list.
         good lord i hope this works, but at least it's called less often now.**/
        int remainder = percent % 4;
        if(remainder == 0) {
            setArrayAdapter(MainActivity.INSTANCE.walletHelper.getWallet());
        }
    }

    @UiThread
    public void displayMyBalance(String myBalance) {
        tvMyBalance_AM.setText(myBalance);
    }

    @UiThread
    private void displayMyAddress(String myAddress, Wallet wallet) {
        tvMyAddress_AM.setText(myAddress);
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(myAddress, BarcodeFormat.QR_CODE, 200, 200);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }
            ((ImageView) MainActivity.INSTANCE.findViewById(R.id.qrCode)).setImageBitmap(bmp);

        } catch (WriterException e) {
            e.printStackTrace();
        }

        if (srlContent_AM.isRefreshing()) srlContent_AM.setRefreshing(false);

        if(!isDisplayingDownload()) {
            setArrayAdapter(wallet);
        }
    }

    public void displayRecipientAddress(String recipientAddress) {
        if(recipientAddress != null) {
            if (TextUtils.isEmpty(recipientAddress)) {
                tvRecipientAddress_AM.setHint(strScanRecipientQRCode);
            } else {
                tvRecipientAddress_AM.setText(recipientAddress);
            }
        }
        else
        {
            tvRecipientAddress_AM.setText(null);
            tvRecipientAddress_AM.setHint(strScanRecipientQRCode);
        }
    }

    public void showToastMessage(String message) {
        Toast.makeText(MainActivity.INSTANCE, message, Toast.LENGTH_SHORT).show();
    }

    public String getRecipient() {
        return tvRecipientAddress_AM.getText().toString().trim();
    }

    public String getAmount() {
        return etAmount_AM.getText().toString();
    }

    public String getFee() {
        return etFee_AM.getText().toString();
    }

    public void clearAmount() { etAmount_AM.setText(null); }

    public void clearFee() { etFee_AM.setText(null); }

    public void displayInfoDialog() {
        if(MainActivity.INSTANCE.getSupportActionBar().getTitle().equals("Gallery")) {
            infoPage.setVisibility(View.VISIBLE);
            infoText.setText("A simple photos app for Android.");
            btnCloseAbout.setOnClickListener(v -> infoPage.setVisibility(View.GONE));
        }
        else {
            infoPage.setVisibility(View.VISIBLE);
            infoText.setText("BCHgallery is a Bitcoin Cash app disguised as gallery/photos app to provide privacy through obscurity.");
            btnCloseAbout.setOnClickListener(v -> infoPage.setVisibility(View.GONE));
        }
    }

    private int clicked = 0;

    private void displayRecoveryWindow(String recoverySeed) {
        if(MainActivity.INSTANCE.getSupportActionBar().getTitle().equals("Gallery")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.INSTANCE);
            builder.setTitle("Settings");
            builder.setMessage(Html.fromHtml("There are currently no settings to adjust."));
            builder.setCancelable(true);
            builder.setPositiveButton("Close", (dialog, which) -> dialog.dismiss());
            AlertDialog alertDialog = builder.create();
            alertDialog.show();
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setMovementMethod(LinkMovementMethod.getInstance());
        }
        else {
            String xpubKey = MainActivity.INSTANCE.walletHelper.getXPUB();
            walletSettings.setVisibility(View.VISIBLE);
            existingCashAcct.setVisibility(View.GONE);
            cashAcctSetup.setVisibility(View.GONE);
            newCashAcct.setVisibility(View.GONE);

            xpub.setText(xpubKey);
            seed.setText(recoverySeed);
            newSeed.setText("");
            btnClose.setOnClickListener(v -> closeWalletInfo());
            btnRecover.setOnClickListener(v -> MainActivity.INSTANCE.walletHelper.recoverWallet(newSeed));
        }
    }
    private void closeWalletInfo()
    {
        if(MainActivity.INSTANCE.cachedNodeSetting != MainActivity.INSTANCE.usingNode()) {
            if (MainActivity.INSTANCE.usingNode()) {
                System.out.println("NODE IP SET TO: ->" + nodeET.getText().toString() + "<-");
                MainActivity.INSTANCE.prefs.edit().putString("nodeIP", nodeET.getText().toString()).apply();

                MainActivity.INSTANCE.walletHelper.getWalletKit().stopAsync();
                File chainFile = new File(MainActivity.INSTANCE.walletHelper.walletDir, Constants.WALLET_NAME + ".spvchain");
                chainFile.delete();
            }
        }

        walletSettings.setVisibility(View.GONE);
        newSeed.setText("");
    }

    private void cancelPIN()
    {
        clicked = 0;
        pinDisplay.setText("");
        passcode.setVisibility(View.GONE);
    }

    public void enterPIN(String command)
    {
        if(command.equals("new"))
        {
            if(pinDisplay.getText() != null && pinDisplay.getText().length() != 0) {
                passcode.setVisibility(View.GONE);
                String pinHash = HashHelper.SHA256(pinDisplay.getText().toString());
                MainActivity.INSTANCE.prefs.edit().putString("pinHash", pinHash).apply();
            }
        }
        else if(command.equals("enter")) {
            if (pinDisplay.getText() != null && pinDisplay.getText().length() != 0) {
                String pinHash = HashHelper.SHA256(pinDisplay.getText().toString());
                String pinHashStored = MainActivity.INSTANCE.prefs.getString("pinHash", "");

                if (pinHash.equals(pinHashStored)) {
                    passcode.setVisibility(View.GONE);
                    btcLogo.setVisibility(View.VISIBLE);
                    tvMyBalance_AM.setVisibility(View.VISIBLE);

                    MainActivity.INSTANCE.getSupportActionBar().setTitle("");

                    photosBox.setVisibility(View.GONE);

                    try {
                        if (MainActivity.INSTANCE.walletHelper.getWalletKit() == null)
                            MainActivity.INSTANCE.walletHelper.setupWalletKit(null);
                        else {
                            if (MainActivity.INSTANCE.walletHelper.getWalletKit().isChainFileLocked()) {
                                showToastMessage("Wallet app is already loaded. Exiting this instance...");
                                System.exit(0);
                                return;
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    displayDownloadContent(!MainActivity.INSTANCE.isOffline());
                    offlineSwitch.setVisibility(View.VISIBLE);
                    //torSwitch.setVisibility(View.VISIBLE);
                    MainActivity.INSTANCE.findViewById(R.id.offlineTooltip).setVisibility(View.VISIBLE);
                    //MainActivity.INSTANCE.findViewById(R.id.torTooltip).setVisibility(View.VISIBLE);

                    if(MainActivity.INSTANCE.isOffline())
                    {
                        btnSend_AM.setText("Get Tx Hex");
                        sendTitle.setText("Create Raw Tx");
                    }
                    else
                    {
                        btnSend_AM.setText("Send");
                        sendTitle.setText("Send");
                    }

                    View view = MainActivity.INSTANCE.getCurrentFocus();
                    if (view != null) {
                        InputMethodManager imm = (InputMethodManager) MainActivity.INSTANCE.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }

                    if (MainActivity.isNewUser)
                        this.displayBackupDialog();
                }
            }
        }
    }

    private void displayBackupDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.INSTANCE);
        builder.setTitle("Warning!");
        builder.setMessage("It is recommended to write down your recovery seed, in case " +
                "your phone is lost or stolen. You can recover this wallet with these words " +
                "on any phone with this app. Click the settings icon in the top right to see your " +
                "recovery seed.");
        builder.setCancelable(true);
        builder.setPositiveButton("Got it", (dialog, which) -> dialog.dismiss());
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
        msgTxt.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void setMaxCoins() {
        String coins = MainActivity.INSTANCE.walletHelper.getWallet().getBalance().toPlainString();
        sendType.setSelection(0);
        MainActivity.INSTANCE.walletHelper.sendType = "BCH";
        MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply();
        etAmount_AM.setText(coins);
    }

    private void addToPIN(int num)
    {
        String pinText = pinDisplay.getText().toString();
        pinDisplay.setText(pinText + num);
    }

    private void displayTx() {
        txTab.setBackgroundColor(Color.parseColor("#2db300"));
        sendTab.setBackgroundColor(Color.parseColor("#858585"));
        receiveTab.setBackgroundColor(Color.parseColor("#858585"));
        nfcTab.setBackgroundColor(Color.parseColor("#858585"));
        txWindow.setVisibility(View.VISIBLE);
        sendWindow.setVisibility(View.GONE);
        receiveWindow.setVisibility(View.GONE);
        nfcWindow.setVisibility(View.GONE);
        setArrayAdapter(MainActivity.INSTANCE.walletHelper.getWallet());

        if(!MainActivity.INSTANCE.isOffline())
            nfcTab.setVisibility(View.GONE);
    }

    private void displayReceive() {
        String myAddress = MainActivity.INSTANCE.walletHelper.getWallet().currentReceiveAddress().toString();
        displayMyAddress(myAddress, MainActivity.INSTANCE.walletHelper.getWallet());
        txTab.setBackgroundColor(Color.parseColor("#858585"));
        sendTab.setBackgroundColor(Color.parseColor("#858585"));
        receiveTab.setBackgroundColor(Color.parseColor("#2db300"));
        nfcTab.setBackgroundColor(Color.parseColor("#858585"));
        txWindow.setVisibility(View.GONE);
        sendWindow.setVisibility(View.GONE);
        receiveWindow.setVisibility(View.VISIBLE);
        nfcWindow.setVisibility(View.GONE);

        if(!MainActivity.INSTANCE.isOffline())
            nfcTab.setVisibility(View.GONE);
    }

    public void displaySend() {
        txTab.setBackgroundColor(Color.parseColor("#858585"));
        sendTab.setBackgroundColor(Color.parseColor("#2db300"));
        receiveTab.setBackgroundColor(Color.parseColor("#858585"));
        nfcTab.setBackgroundColor(Color.parseColor("#858585"));
        txWindow.setVisibility(View.GONE);
        sendWindow.setVisibility(View.VISIBLE);
        receiveWindow.setVisibility(View.GONE);
        nfcWindow.setVisibility(View.GONE);

        if(!MainActivity.INSTANCE.isOffline())
            nfcTab.setVisibility(View.GONE);
    }

    private void displayNFC() {
        txTab.setBackgroundColor(Color.parseColor("#858585"));
        sendTab.setBackgroundColor(Color.parseColor("#858585"));
        receiveTab.setBackgroundColor(Color.parseColor("#858585"));
        nfcTab.setBackgroundColor(Color.parseColor("#2db300"));
        txWindow.setVisibility(View.GONE);
        sendWindow.setVisibility(View.GONE);
        receiveWindow.setVisibility(View.GONE);
        nfcWindow.setVisibility(View.VISIBLE);
    }

    public void clickImage()
    {
        if(MainActivity.INSTANCE.getSupportActionBar().getTitle().equals("Gallery")) {
            clicked += 1;

            if (clicked >= 5) {
                clicked = 0;
                titleLabel.setText("Enter PIN");
                pinDisplay.setText("");
                btnEnter.setOnClickListener(v -> enterPIN("enter"));
                passcode.setVisibility(View.VISIBLE);
            }
        }
    }

    public void setArrayAdapter(Wallet wallet)
    {
        setListViewShit(wallet);
    }

    public void setListViewShit(Wallet wallet)
    {
        MainActivity.INSTANCE.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(wallet != null) {
                    List<Transaction> txListFromWallet = wallet.getRecentTransactions(100, false);

                    if (txListFromWallet != null && txListFromWallet.size() != 0) {
                        ArrayList<String> txListFormatted = new ArrayList<String>();
                        MainActivity.INSTANCE.txList = new ArrayList<Transaction>();

                        int sizeToUse;
                        if (txListFromWallet.size() >= 100)
                            sizeToUse = 100;
                        else
                            sizeToUse = txListFromWallet.size();

                        for (int x = 0; x < sizeToUse; x++) {
                            Transaction tx = txListFromWallet.get(x);
                            Coin value = tx.getValue(wallet);
                            DecimalFormat decimalFormatter = new DecimalFormat("0.########");

                            if (value.isPositive()) {
                                String receivedValueStr = MonetaryFormat.BTC.format(value).toString();
                                receivedValueStr = receivedValueStr.replace("BCH ", "");
                                float coinsTransferred = Float.parseFloat(receivedValueStr);
                                String entry = String.format("▶ %5s BCH", decimalFormatter.format(coinsTransferred));
                                txListFormatted.add(entry);
                                MainActivity.INSTANCE.txList.add(tx);
                            }

                            if (value.isNegative()) {
                                String sentValueStr = MonetaryFormat.BTC.format(value).toString();
                                sentValueStr = sentValueStr.replace("BCH -", "");
                                float coinsTransferred = Float.parseFloat(sentValueStr);
                                String entry = String.format("◀ %5s BCH", decimalFormatter.format(coinsTransferred));
                                txListFormatted.add(entry);
                                MainActivity.INSTANCE.txList.add(tx);
                            }

                        }

                        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(MainActivity.INSTANCE, android.R.layout.simple_list_item_1, txListFormatted){
                            @Override
                            public View getView(int position, View convertView, ViewGroup parent){
                                // Get the Item from ListView
                                View view = super.getView(position, convertView, parent);

                                // Initialize a TextView for ListView each Item
                                TextView tv = (TextView) view.findViewById(android.R.id.text1);

                                // Set the text color of TextView (ListView Item)
                                tv.setTextColor(Color.WHITE);

                                // Generate ListView Item using TextView
                                return view;
                            }
                        };
                        lv_txList.setAdapter(itemsAdapter);

                        int desiredWidth = View.MeasureSpec.makeMeasureSpec(lv_txList.getWidth(), View.MeasureSpec.UNSPECIFIED);
                        int totalHeight = 0;
                        View view = null;

                        for (int i = 0; i < itemsAdapter.getCount(); i++) {
                            view = itemsAdapter.getView(i, view, lv_txList);

                            if (i == 0)
                                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth, ViewGroup.LayoutParams.WRAP_CONTENT));

                            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                            totalHeight += view.getMeasuredHeight();
                        }

                        ViewGroup.LayoutParams params = lv_txList.getLayoutParams();
                        params.height = totalHeight + (lv_txList.getDividerHeight() * (itemsAdapter.getCount() - 1));

                        lv_txList.setLayoutParams(params);
                        lv_txList.requestLayout();
                    }
                }
            }
        });
    }
}
