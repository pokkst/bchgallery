package xyz.pokkst.gallerybch.json;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Random;

public class JSONHelper {

    public JSONHelper()
    {}

    public static String readJSONFile(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public String getCashAccountIdentifier(String cashAccount)
    {
        String[] cashAcctServers = new String[]{
                "https://cashacct.imaginary.cash",
                "https://electrum.imaginary.cash",
                "https://cashaccounts.bchdata.cash",
                "https://cashacct.electroncash.dk",
                "https://rest.bitcoin.com/v2/cashAccounts"
        };


        int randExplorer = new Random().nextInt(cashAcctServers.length);
        String lookupServer = cashAcctServers[randExplorer];

        String identity = "";

        String[] splitAccount = cashAccount.split("#");
        String name = splitAccount[0];
        String block = splitAccount[1];

        InputStream is = null;
        try {
            if(!lookupServer.contains("rest.bitcoin.com")) {
                if(block.contains("."))
                {
                    String[] splitBlock = block.split("\\.");
                    String mainBlock = splitBlock[0];
                    String blockCollision = splitBlock[1];

                    is = new URL(lookupServer + "/account/" + mainBlock + "/" + name + "/" + blockCollision).openStream();
                }
                else
                {
                    is = new URL(lookupServer + "/account/" + block + "/" + name).openStream();
                }
            }
            else
            {
                if(block.contains("."))
                {
                    String[] splitBlock = block.split("\\.");
                    String mainBlock = splitBlock[0];
                    String blockCollision = splitBlock[1];

                    is = new URL(lookupServer + "/lookup/" + name + "/" + mainBlock + "/" + blockCollision).openStream();
                }
                else
                {
                    is = new URL(lookupServer + "/lookup/" + name + "/" + block).openStream();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = JSONHelper.readJSONFile(rd);
            JSONObject json = new JSONObject(jsonText);
            identity = json.getString("identifier").replace(";","");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return identity;
    }

    public String getTransactionsBlock(String transactionHash)
    {
        String[] blockExplorers = new String[]{
                "btc.com",
                "blockdozer.com",
                "coin.space"
        };
        String[] blockExplorerAPIURL = new String[]{
                "https://bch-chain.api.btc.com/v3/tx/",
                "https://blockdozer.com/api/tx/",
                "https://bch.coin.space/api/tx/"
        };

        int randExplorer = new Random().nextInt(blockExplorers.length);
        String blockExplorer = blockExplorers[randExplorer];
        String blockExplorerURL = blockExplorerAPIURL[randExplorer];

        String block = "";
        String txHash = transactionHash.toLowerCase();
        InputStream is = null;
        try {
            is = new URL(blockExplorerURL + txHash).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = JSONHelper.readJSONFile(rd);
            JSONObject json = new JSONObject(jsonText);

            if(blockExplorer.equals("btc.com")) {
                block = json.getJSONObject("data").getString("block_height");
            }
            else if(blockExplorer.equals("blockdozer.com") || blockExplorer.equals("coin.space"))
            {
                block = json.getString("blockheight");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return block.equals("-1") ? "???" : block;
    }

    public String getTransactionsBlockHash(String transactionHash)
    {
        String[] blockExplorers = new String[]{
                "btc.com",
                "blockdozer.com",
                "coin.space"
        };
        String[] blockExplorerAPIURL = new String[]{
                "https://bch-chain.api.btc.com/v3/tx/",
                "https://blockdozer.com/api/tx/",
                "https://bch.coin.space/api/tx/"
        };

        int randExplorer = new Random().nextInt(blockExplorers.length);
        String blockExplorer = blockExplorers[randExplorer];
        String blockExplorerURL = blockExplorerAPIURL[randExplorer];

        String block = "";
        String txHash = transactionHash.toLowerCase();
        InputStream is = null;
        try {
            is = new URL(blockExplorerURL + txHash).openStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = JSONHelper.readJSONFile(rd);
            JSONObject json = new JSONObject(jsonText);

            if(blockExplorer.equals("btc.com")) {
                block = json.getJSONObject("data").getString("block_hash");
            }
            else if(blockExplorer.equals("blockdozer.com") || blockExplorer.equals("coin.space"))
            {
                block = json.getString("blockhash");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return block.equals("-1") ? "???" : block;
    }

    public String getRegisterTxHash(String jsonResponse)
    {
        String hash = "";
        try {
            JSONObject json = new JSONObject(jsonResponse);
            hash = json.getString("txid");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return hash;
    }
}
