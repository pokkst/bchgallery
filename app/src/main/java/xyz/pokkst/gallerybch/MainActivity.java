package xyz.pokkst.gallerybch;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.bitcoinj.core.Coin;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.protocols.payments.PaymentProtocolException;
import org.bitcoinj.protocols.payments.PaymentSession;
import org.bitcoinj.utils.MonetaryFormat;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import xyz.pokkst.gallerybch.android.PermissionHelper;
import xyz.pokkst.gallerybch.crypto.HashHelper;
import xyz.pokkst.gallerybch.json.JSONHelper;
import xyz.pokkst.gallerybch.net.NetHelper;
import xyz.pokkst.gallerybch.nfc.NFCHelper;
import xyz.pokkst.gallerybch.qr.QRHelper;
import xyz.pokkst.gallerybch.ui.UIHelper;
import xyz.pokkst.gallerybch.uri.URIHelper;
import xyz.pokkst.gallerybch.util.Constants;
import xyz.pokkst.gallerybch.wallet.WalletHelper;

public class MainActivity extends AppCompatActivity {

    public static MainActivity INSTANCE;
    public UIHelper uiHelper;
    public WalletHelper walletHelper;
    public NFCHelper nfcHelper;


    public ArrayList<Transaction> txList;
    public SharedPreferences prefs = null;
    public static boolean isNewUser = true;
    private boolean offline = false;
    private boolean tor = false;
    private boolean customNode = false;
    public boolean changedNodeSetting = false;
    public boolean cachedNodeSetting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changedNodeSetting = false;
        INSTANCE = this;
        setContentView(R.layout.activity_main);
        this.nfcHelper = new NFCHelper(this);
        this.uiHelper = new UIHelper();
        this.walletHelper = new WalletHelper();
        this.nfcHelper.readFromIntent(getIntent());
        initToolbar();

        //Cash Account name: apptest5
        //Tx hash: a58fb417607cda1bdfd382b53296129ca2f32a03c9d6b42a22a5687fb8f3fc11
        //Block hash: 0000000000000000035c2bbc751b91e95458b7da92819577cbd21a25c2d0cfa5
        //Account minimal identifier: 12588
        //Collision identifier expected: 6176006803
        //Collision identifier: 7463847412

        //Cash Account name:
        //Tx hash: 590d1fdf7e04af0ee08f9194bb9e8d1971bdcbf55d29303d5bf32d4eae5e7136
        //Block hash: 000000000000000002abbeff5f6fb22a0b3b5c2685c6ef4ed2d2257ed54e9dcb
        //Account minimal identifier:
        //Collision identifier expected: 5876958390
        //Collision identifier: 5876958390
        prefs = getSharedPreferences("xyz.pokkst.gallerybch", MODE_PRIVATE);

        if (prefs.getBoolean("isNewUser", true)) {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            isNewUser = true;

            prefs.edit().putBoolean("offlineMode", false).apply();
            offline = false;
            this.uiHelper.offlineSwitch.setChecked(offline);

            prefs.edit().putBoolean("usingTor", false).apply();
            tor = false;
            this.uiHelper.torSwitch.setChecked(tor);

            prefs.edit().putBoolean("customNode", false).apply();
            customNode = false;
            this.uiHelper.nodeSwitch.setChecked(customNode);

            cachedNodeSetting = customNode;

            prefs.edit().putString("sendType", "BCH").apply();
            this.walletHelper.sendType = "BCH";
            this.uiHelper.initSendType();

            prefs.edit().putBoolean("isNewUser", false).apply();
        }
        else
        {
            offline = prefs.getBoolean("offlineMode", false);
            this.uiHelper.offlineSwitch.setChecked(offline);

            tor = prefs.getBoolean("usingTor", false);
            this.uiHelper.torSwitch.setChecked(tor);

            customNode = prefs.getBoolean("customNode", false);
            this.uiHelper.nodeSwitch.setChecked(customNode);

            this.walletHelper.sendType = prefs.getString("sendType", "BCH");
            this.uiHelper.initSendType();

            String nodeIP = MainActivity.INSTANCE.prefs.getString("nodeIP", "");

            assert nodeIP != null;
            if(!nodeIP.equals("")) {
                this.uiHelper.nodeET.setText(nodeIP);
            }

            String cashAcct = MainActivity.INSTANCE.prefs.getString("cashAccount", "");

            assert cashAcct != null;
            if(!cashAcct.equals("") && cashAcct.contains("#???")) {
                String plainName = cashAcct.replace("#???", "");
                this.uiHelper.registeredBlock = "???";
                String cashAcctTx = MainActivity.INSTANCE.prefs.getString("cashAcctTx", "null");
                this.uiHelper.registeredTxHash = cashAcctTx;
                this.uiHelper.initialAccountIdentityCheck(plainName);

                this.uiHelper.timer = new CountDownTimer(150000, 20) {

                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        try{
                            uiHelper.checkForAccountIdentity();
                        }catch(Exception e){
                            Log.e("Error", "Error: " + e.toString());
                        }
                    }
                }.start();
            }

            cachedNodeSetting = customNode;

            isNewUser = false;
        }

        if(usingNode())
        {
            this.uiHelper.nodeET.setVisibility(View.VISIBLE);
        }
        else
        {
            this.uiHelper.nodeET.setVisibility(View.GONE);
        }

        this.uiHelper.offlineSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            offline = isChecked;
            prefs.edit().putBoolean("offlineMode", offline).apply();
        });

        this.uiHelper.torSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            tor = isChecked;
            prefs.edit().putBoolean("usingTor", tor).apply();
        });

        this.uiHelper.nodeSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> {
            customNode = isChecked;

            if(customNode)
            {
                this.uiHelper.nodeET.setVisibility(View.VISIBLE);
            }
            else
            {
                this.uiHelper.nodeET.setVisibility(View.GONE);
            }

            prefs.edit().putBoolean("customNode", customNode).apply();
        });

        this.uiHelper.displaySend();

        if(isNewUser) {
            this.uiHelper.titleLabel.setText("Enter New PIN");
            this.uiHelper.pinDisplay.setText("");
            this.uiHelper.passcode.setVisibility(View.VISIBLE);
            this.uiHelper.btnEnter.setOnClickListener(v -> this.uiHelper.enterPIN("new"));
        }

        PermissionHelper permissionHelper = new PermissionHelper();
        permissionHelper.askForPermissions(this, this);


        this.uiHelper.toolbar_AT.setOnMenuItemClickListener(item -> {

            if(item.getItemId()==R.id.menuInfo_MM)
            {
                this.uiHelper.displayInfoDialog();
            }

            if(item.getItemId()== R.id.menuRecovery_MM)
            {
                this.uiHelper.getRecoveryWindow();
            }

            if(item.getItemId()== R.id.qrScan)
            {
                QRHelper qrHelper = new QRHelper();
                qrHelper.startQRScan(MainActivity.INSTANCE);
            }

            return true;
        });
    }

    private void initToolbar() {
        setSupportActionBar(this.uiHelper.toolbar_AT);
        if(getSupportActionBar() != null) {
            this.uiHelper.btcLogo.setVisibility(View.GONE);
            this.uiHelper.tvMyBalance_AM.setVisibility(View.GONE);
            getSupportActionBar().setTitle("Gallery");
            //getSupportActionBar().setTitle(Constants.APP_NAME + " " + Constants.APP_VERSION);
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode != 14)
        {
            IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (scanResult != null) {
                String address = scanResult.getContents();
                if (address != null) {
                    MainActivity.INSTANCE.walletHelper.processScanOrPaste(address);
                } else {
                    if (address.contains("?")) {
                        new URIHelper().handleURI(address);
                    } else {
                        this.uiHelper.displayRecipientAddress(address);
                    }
                }
            }
        }
        else
        {
            if(data != null) {
                if (data.getData() != null) {
                    Uri contactData = data.getData();
                    Cursor c = getContentResolver().query(contactData, null, null, null, null);
                    assert c != null;
                    if (c.moveToFirst()) {
                        int phoneIndex = c.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER);
                        String num = c.getString(phoneIndex);
                        uiHelper.displayRecipientAddress(num);
                        c.close();
                    }
                } else {
                    uiHelper.showToastMessage("No contact selected.");
                }
            } else {
                uiHelper.showToastMessage("No contact selected.");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(nfcHelper.getNfcAdapter() != null)
            nfcHelper.getNfcAdapter().enableForegroundDispatch(this, nfcHelper.getPendingIntent(), nfcHelper.getWriteTagFilters(), null);
    }

    @Override
    public void onPause(){
        super.onPause();

        if(nfcHelper.getNfcAdapter() != null)
            nfcHelper.getNfcAdapter().disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);

        if(nfcHelper.getNfcAdapter() != null) {
            nfcHelper.readFromIntent(intent);
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
                Tag newTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                nfcHelper.setNfcTag(newTag);
            }
        }
    }

    public boolean isOffline()
    {
        return offline;
    }

    public boolean usingNode()
    {
        return customNode;
    }
}
