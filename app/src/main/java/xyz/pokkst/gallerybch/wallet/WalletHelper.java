package xyz.pokkst.gallerybch.wallet;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Handler;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.vdurmont.emoji.EmojiParser;

import org.bitcoinj.core.Address;
import org.bitcoinj.core.AddressFactory;
import org.bitcoinj.core.AddressFormatException;
import org.bitcoinj.core.Base58;
import org.bitcoinj.core.BlockChain;
import org.bitcoinj.core.Coin;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;
import org.bitcoinj.core.InsufficientMoneyException;
import org.bitcoinj.core.NetworkParameters;
import org.bitcoinj.core.PeerAddress;
import org.bitcoinj.core.PeerGroup;
import org.bitcoinj.core.Sha256Hash;
import org.bitcoinj.core.Transaction;
import org.bitcoinj.core.listeners.DownloadProgressTracker;
import org.bitcoinj.kits.WalletAppKit;
import org.bitcoinj.net.discovery.PeerDiscovery;
import org.bitcoinj.net.discovery.PeerDiscoveryException;
import org.bitcoinj.params.MainNetParams;
import org.bitcoinj.params.TestNet3Params;
import org.bitcoinj.protocols.payments.PaymentProtocol;
import org.bitcoinj.protocols.payments.PaymentProtocolException;
import org.bitcoinj.protocols.payments.PaymentSession;
import org.bitcoinj.script.Script;
import org.bitcoinj.store.BlockStoreException;
import org.bitcoinj.store.MemoryBlockStore;
import org.bitcoinj.utils.BriefLogFormatter;
import org.bitcoinj.utils.MonetaryFormat;
import org.bitcoinj.utils.Threading;
import org.bitcoinj.wallet.DeterministicSeed;
import org.bitcoinj.wallet.SendRequest;
import org.bitcoinj.wallet.Wallet;
import org.spongycastle.crypto.util.PrivateKeyFactory;
import org.spongycastle.crypto.util.PrivateKeyInfoFactory;
import org.spongycastle.util.encoders.Hex;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.security.PrivateKey;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import xyz.pokkst.gallerybch.MainActivity;
import xyz.pokkst.gallerybch.R;
import xyz.pokkst.gallerybch.json.JSONHelper;
import xyz.pokkst.gallerybch.net.NetHelper;
import xyz.pokkst.gallerybch.ui.UIHelper;
import xyz.pokkst.gallerybch.uri.URIHelper;
import xyz.pokkst.gallerybch.util.Constants;

public class WalletHelper {
    public File walletDir;

    public NetworkParameters parameters;
    private WalletAppKit walletAppKit;
    public String serializedXpub;
    private UIHelper uiHelper;
    public String sendType;

    public WalletHelper()
    {
        this.walletDir = new File(MainActivity.INSTANCE.getApplicationInfo().dataDir);
        parameters = Constants.IS_PRODUCTION ? MainNetParams.get() : TestNet3Params.get();
        BriefLogFormatter.init();

        uiHelper = MainActivity.INSTANCE.uiHelper;
    }

    public void setupWalletKit(DeterministicSeed seed)
    {
        setBitcoinSDKThread();
        walletAppKit = new WalletAppKit(parameters, walletDir, Constants.WALLET_NAME) {
            @Override
            protected void onSetupCompleted() {
                wallet().allowSpendingUnconfirmedTransactions();
                setupWalletListeners(wallet());
                serializedXpub = wallet().getWatchingKey().serializePubB58(parameters);

                if(MainActivity.INSTANCE.isOffline())
                    MainActivity.INSTANCE.uiHelper.refresh();
            }
        };

        if(MainActivity.INSTANCE.isOffline()) {
            walletAppKit.setPeerNodes(null);
            walletAppKit.setDiscovery(new PeerDiscovery() {
                @Override
                public InetSocketAddress[] getPeers(long l, long l1, TimeUnit timeUnit) throws PeerDiscoveryException {
                    return null;
                }

                @Override
                public void shutdown() {

                }
            });
        }
        else {
            //Use this to connect to the hardfork upgrade testnet

            /*walletAppKit.setPeerNodes(null);
            walletAppKit.setDiscovery(new PeerDiscovery() {
                @Override
                public InetSocketAddress[] getPeers(long l, long l1, TimeUnit timeUnit) throws PeerDiscoveryException {
                    return null;
                }

                @Override
                public void shutdown() {

                }
            });
            InetAddress node_testnet = null;

            try {
                //IP of testnet.imaginary.cash node
                node_testnet = InetAddress.getByName("70.36.125.75");
                walletAppKit.setPeerNodes(new PeerAddress(node_testnet, 18333));

            } catch (UnknownHostException e) {
                e.printStackTrace();
            }*/

            if(MainActivity.INSTANCE.usingNode())
            {
                String nodeIP = MainActivity.INSTANCE.prefs.getString("nodeIP", "");
                System.out.println("GETTING NODE IP: " + nodeIP);
                assert nodeIP != null;
                if(!nodeIP.equals("")) {
                    walletAppKit.setPeerNodes(null);
                    walletAppKit.setDiscovery(new PeerDiscovery() {
                        @Override
                        public InetSocketAddress[] getPeers(long l, long l1, TimeUnit timeUnit) throws PeerDiscoveryException {
                            return null;
                        }

                        @Override
                        public void shutdown() {

                        }
                    });
                    InetAddress node1 = null;

                    try {
                        node1 = InetAddress.getByName(nodeIP);
                    } catch (UnknownHostException e) {
                        e.printStackTrace();
                    }

                    PeerAddress[] addresses = new PeerAddress[]{
                            new PeerAddress(node1, Constants.IS_PRODUCTION ? 8333 : 18333)
                    };

                    walletAppKit.setPeerNodes(addresses);
                }
            }

            walletAppKit.setDownloadListener(new DownloadProgressTracker() {
                @Override
                protected void progress(double pct, int blocksSoFar, Date date) {
                    super.progress(pct, blocksSoFar, date);
                    int percentage = (int) pct;
                    MainActivity.INSTANCE.runOnUiThread(() -> {
                        uiHelper.displayPercentage(percentage);
                        uiHelper.displayProgress(percentage);
                    });

                }

                @Override
                protected void doneDownload() {
                    super.doneDownload();
                    MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.displayDownloadContent(false));

                    //Used to check that we are on the right chain. Check against https://explorer.bitcoin.com and http://testnet.imaginary.cash
                    System.out.println("Last seen block hash: " + getLatestBlockHash());
                    uiHelper.refresh();
                }
            });
        }

        if(seed != null)
            walletAppKit.restoreWalletFromSeed(seed);

        walletAppKit.setBlockingStartup(false);
        walletAppKit.startAsync();
    }

    public Wallet getWallet()
    {
        return walletAppKit.wallet();
    }

    private String getLatestBlockHash()
    {
        return getWallet().getLastBlockSeenHash().toString();
    }

    public WalletAppKit getWalletKit()
    {
        return walletAppKit;
    }

    public String getXPUB()
    {
        return serializedXpub;
    }

    public boolean isAddressMine(String address)
    {
        Address addressObj = AddressFactory.create().getAddress(parameters, address);

        return getWallet().isPubKeyHashMine(addressObj.getHash160());
    }

    private void setBitcoinSDKThread() {
        final Handler handler = new Handler();
        Threading.USER_THREAD = handler::post;
    }

    public Coin getBalance(Wallet wallet)
    {
        return wallet.getBalance(Wallet.BalanceType.ESTIMATED);
    }

    public void send() {
        if(!uiHelper.isDisplayingDownload()) {
            String amount = uiHelper.getAmount();
            double amtDblToFrmt;

            if (!TextUtils.isEmpty(amount)) {
                amtDblToFrmt = Double.parseDouble(amount);
            } else {
                amtDblToFrmt = 0;
            }

            if(this.sendType.equals("USD") || this.sendType.equals("EUR"))
            {
                if(this.sendType.equals("USD"))
                {
                    new Thread()
                    {
                        @Override
                        public void run()
                        {
                            double usdToBch = amtDblToFrmt / new NetHelper().getBchPriceUsd();
                            System.out.println(usdToBch);
                            DecimalFormat df = new DecimalFormat("#.########");
                            String amtDblFrmt = df.format(usdToBch);
                            System.out.println(amtDblFrmt);
                            processPayment(usdToBch+"", amtDblFrmt);
                        }
                    }.start();
                }

                if(this.sendType.equals("EUR"))
                {
                    new Thread()
                    {
                        @Override
                        public void run()
                        {
                            double eurToBch = amtDblToFrmt / new NetHelper().getBchPriceEur();
                            System.out.println(eurToBch);
                            DecimalFormat df = new DecimalFormat("#.########");
                            String amtDblFrmt = df.format(eurToBch);
                            System.out.println(amtDblFrmt);
                            processPayment(eurToBch+"", amtDblFrmt);
                        }
                    }.start();
                }
            }
            else
            {
                DecimalFormat df = new DecimalFormat("#.########");
                String amtDblFrmt = df.format(amtDblToFrmt);
                processPayment(amount, amtDblFrmt);
            }


        }
    }

    private void processPayment(String amount, String amtDblFrmt) {
        System.out.println(amount + " " + amtDblFrmt);
        if (TextUtils.isEmpty(uiHelper.getRecipient())) {
            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Please enter a proper recipient."));
        } else if (TextUtils.isEmpty(amount) || Double.parseDouble(amtDblFrmt) <= 0.0000001) {
            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Enter a valid amount. Minimum is 0.0000001 BCH"));
        } else {
            String receiverAddress = uiHelper.getRecipient();

            if(receiverAddress.startsWith("+") || receiverAddress.startsWith("cointext:")) {
                String rawNumber = this.getRawPhoneNumber(receiverAddress);
                String numberString = rawNumber.replace("+", "");
                double amtToSats = java.lang.Double.parseDouble(amount);
                DecimalFormat satFormatter = new DecimalFormat("#", new DecimalFormatSymbols(Locale.US));
                amtToSats *= 100000000;
                int sats = Integer.parseInt(satFormatter.format(amtToSats));
                String url = "https://pay.cointext.io/p/" + numberString + "/" + sats;
                System.out.println(url);
                this.processBIP70(url);
            } else if (receiverAddress.startsWith("https://") || receiverAddress.startsWith(parameters.getCashAddrPrefix() + ":?r=https://")) {
                String url = receiverAddress.replace(parameters.getCashAddrPrefix() + ":?r=", "");
                this.processBIP70(url);
            } else {
                //If it contains a pound sign, then we can safely assume it's a Cash Account. Like example#100
                String toAddress = uiHelper.getRecipient();

                if (toAddress.contains("#")) {
                    String toAddressFixed = EmojiParser.removeAllEmojis(toAddress);
                    String toAddressStripped = toAddressFixed.replace("; ", "");

                    String cashAccount = MainActivity.INSTANCE.prefs.getString("cashAccount", "");

                    if (!toAddressStripped.equals(cashAccount)) {
                        sendCoins(amtDblFrmt, toAddressStripped);
                    } else {
                        MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("You cannot send to yourself!"));
                    }
                } else {
                    try {
                        boolean isMyAddress = isAddressMine(uiHelper.getRecipient());

                        if (!isMyAddress) {
                            sendCoins(amtDblFrmt, toAddress);
                        } else {
                            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("You cannot send to yourself!"));
                        }
                    } catch (AddressFormatException e) {
                        e.printStackTrace();
                        MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Invalid address!"));
                    }

                }
            }
        }
    }

    public void processScanOrPaste(String text)
    {
        if (text.contains(MainActivity.INSTANCE.walletHelper.parameters.getCashAddrPrefix())) {
            if (text.contains("?r=")) {
                MainActivity.INSTANCE.uiHelper.sendType.setSelection(0);
                MainActivity.INSTANCE.walletHelper.sendType = MonetaryFormat.CODE_BTC;
                MainActivity.INSTANCE.prefs.edit().putString("sendType", MainActivity.INSTANCE.walletHelper.sendType).apply();

                String addressFixed = text.replace(Constants.IS_PRODUCTION ? "bitcoincash:" : "bchtest:", "");

                String addressFixed2 = addressFixed.replace("?r=", "");
                MainActivity.INSTANCE.uiHelper.displayRecipientAddress(addressFixed2);
                String url = addressFixed2;
                ListenableFuture<PaymentSession> future;

                if (url.startsWith("https")) {
                    try {
                        future = PaymentSession.createFromUrl(url);

                        PaymentSession session = future.get();

                        Coin amountWanted = session.getValue();
                        MainActivity.INSTANCE.runOnUiThread(() -> MainActivity.INSTANCE.uiHelper.etAmount_AM.setText(amountWanted.toPlainString()));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    } catch (PaymentProtocolException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                if (text.contains("?")) {
                    new URIHelper().handleURI(text);
                } else {
                    MainActivity.INSTANCE.uiHelper.displayRecipientAddress(text);
                }
            }
        }
        else if(text.startsWith("cointext:"))
        {
            String numberString = this.getRawPhoneNumber(text);
            MainActivity.INSTANCE.uiHelper.displayRecipientAddress(numberString);
        }
        else
        {
            if (text.contains("?")) {
                new URIHelper().handleURI(text);
            } else {
                MainActivity.INSTANCE.uiHelper.displayRecipientAddress(text);
            }
        }

        if(text.contains("#")) {
            String clipboardStripped = EmojiParser.removeAllEmojis(text);
            String clipboardFinal = clipboardStripped.replace("; ", "");
            MainActivity.INSTANCE.uiHelper.displayRecipientAddress(clipboardFinal);
        }
    }

    private void processBIP70(String url)
    {
        try {
            ListenableFuture<PaymentSession> future = PaymentSession.createFromUrl(url);

            PaymentSession session = future.get();
            if (session.isExpired()) {
                MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Invoice expired!"));
            }

            SendRequest req = session.getSendRequest();
            getWallet().completeTx(req);

            ListenableFuture<PaymentProtocol.Ack> ack = session.sendPayment(ImmutableList.of(req.tx), getWallet().freshReceiveAddress(), null);
            Futures.addCallback(ack, new FutureCallback<PaymentProtocol.Ack>() {
                @Override
                public void onSuccess(@Nullable PaymentProtocol.Ack ack) {
                    getWallet().commitTx(req.tx);
                    MainActivity.INSTANCE.runOnUiThread(()->{
                        uiHelper.tvRecipientAddress_AM.setText(null);
                        uiHelper.etAmount_AM.setText(null);
                    });
                }

                @Override
                public void onFailure(Throwable throwable) {
                    MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("An error occurred."));
                }
            });
        } catch (PaymentProtocolException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InsufficientMoneyException e) {
            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("You do not have enough BCH!"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void sendCoins(String amount, String toAddress) {
        if (toAddress.contains("#") || Address.isValidCashAddr(parameters, toAddress) || Address.isValidLegacyAddress(parameters, toAddress)) {
            new Thread() {
                @Override
                public void run() {
                    String recipientAddress = uiHelper.getRecipient();
                    Coin coinAmt = Coin.parseCoin(amount);

                    if (coinAmt.getValue() > 0.0) {
                        try {
                            SendRequest req;

                            if (coinAmt.equals(getBalance(getWallet())) || coinAmt.isGreaterThan(getBalance(getWallet()))) {
                                req = SendRequest.emptyWallet(parameters, recipientAddress);
                            } else {
                                req = SendRequest.to(parameters, recipientAddress, coinAmt);
                            }

                            req.ensureMinRequiredFee = false;

                            req.feePerKb = Coin.valueOf(java.lang.Long.parseLong("1") * 1000L);

                            getWallet().sendCoins(getWalletKit().peerGroup(), req);
                        } catch (InsufficientMoneyException e) {
                            e.printStackTrace();
                            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage(e.getMessage()));
                        } catch (Wallet.CouldNotAdjustDownwards e) {
                            e.printStackTrace();
                            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Not enough BCH for fee!"));
                        } catch (Wallet.ExceededMaxTransactionSize e) {
                            e.printStackTrace();
                            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Transaction is too large!"));
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Cash Account not found."));
                        }

                    }
                }
            }.start();
        } else if (!Address.isValidCashAddr(parameters, toAddress) || !Address.isValidLegacyAddress(parameters, toAddress)) {
            MainActivity.INSTANCE.runOnUiThread(() -> uiHelper.showToastMessage("Invalid address!"));
        }
    }

    private String getRawPhoneNumber(String address) {
        String cointextString = address.replace("cointext:", "");
        String removedDashes = cointextString.replace("-", "");
        String removedOpenParenthesis = removedDashes.replace("(", "");
        String removedClosedParenthesis = removedOpenParenthesis.replace(")", "");
        String number = removedClosedParenthesis.replace(".", "");

        if(!number.contains("+"))
        {
            number = "+1$number";
        }

        return number;
    }

    public void recoverWallet(EditText newSeed)
    {
        if (newSeed.getText() != null && !TextUtils.isEmpty(newSeed.getText())) {
            /*We use this as the creation time, since it's shortly before the app was released, so no wallets of this app exist beforehand,
              thus checking wallets for transactions would be useless.
             */
            long creationTime = 1537401600L;
            DeterministicSeed seed = new DeterministicSeed(Splitter.on(' ').splitToList(newSeed.getText()), null, "", creationTime);

            int length = Splitter.on(' ').splitToList(newSeed.getText()).size();

            if (length == 12) {
                MainActivity.INSTANCE.uiHelper.walletSettings.setVisibility(View.GONE);
                newSeed.setText("");
                restoreWallet(seed);
                MainActivity.INSTANCE.uiHelper.showToastMessage("Recovery seed entered. Restoring...");
            }
        } else {
            MainActivity.INSTANCE.uiHelper.showToastMessage("Recovery seed not entered.");
        }
    }

    private void restoreWallet(DeterministicSeed seed)
    {
        walletAppKit.stopAsync();
        walletAppKit = null;
        MainActivity.INSTANCE.uiHelper.displayDownloadContent(true);
        File walletFile = new File(walletDir, Constants.WALLET_NAME + ".wallet");
        walletFile.delete();
        File chainFile = new File(walletDir, Constants.WALLET_NAME + ".spvchain");
        chainFile.delete();

        setupWalletKit(seed);
    }

    private void setupWalletListeners(Wallet wallet) {
        wallet.addCoinsReceivedEventListener((wallet1, tx, prevBalance, newBalance) -> {
            if(!uiHelper.isDisplayingDownload()) {
                uiHelper.displayMyBalance(getBalance(wallet1).toFriendlyString());

                if (tx.getPurpose() == Transaction.Purpose.UNKNOWN)
                    uiHelper.showToastMessage("Received coins!");

                uiHelper.refresh();
            }
        });

        wallet.addCoinsSentEventListener((wallet12, tx, prevBalance, newBalance) -> {
            if(!uiHelper.isDisplayingDownload() && !MainActivity.INSTANCE.isOffline()) {
                uiHelper.displayMyBalance(getBalance(wallet12).toFriendlyString());
                uiHelper.clearAmount();
                uiHelper.displayRecipientAddress(null);
                uiHelper.showToastMessage("Sent coins!");
                uiHelper.refresh();
            }
        });
    }
}
